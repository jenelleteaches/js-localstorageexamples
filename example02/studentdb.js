
// This function gets run when the person presses the SAVE button in the UI
function save() {
  // 1. Get what the person typed in the textbox
  let name = document.getElementById("nameInput").value;
  let id = document.getElementById("idInput").value;
  let program = document.getElementById("programInput").value;

  // 2. OPTIONAL:  Do some error handling to check if the results are blank
  if (name.length == 0 || id.length == 0 || program.length == 0) {
    // user didn't enter anything, so show error message and exit
    document.getElementById("results").innerHTML = "Error: You must enter all information"
    return
  }

  // 3. Add data to local storage
  localStorage.setItem("studentName", name);
  localStorage.setItem("studentId", id);
  localStorage.setItem("studentProgram", program);

  console.log(name + " saved to database");

  // 4. Notify the user that the person was saved to the database
  document.getElementById("results").innerHTML = name + " saved to database."
}
