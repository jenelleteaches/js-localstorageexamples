// 1. Get the name out of localStorage
let name = localStorage.getItem("studentName");
let id = localStorage.getItem("studentId");
let program = localStorage.getItem("studentProgram");

// 2. REQUIRED ERROR HANDLING:  Check if data exists in database
if (name == undefined || id ==  undefined || program == undefined) {
  // 3a. no items found in local storage
  document.getElementById("results").innerHTML = "No information in database."
}
else {
  // 3b. 'studentName' key exists in localStorage, so output it to UI
  document.getElementById("results").innerHTML = "Name: " + name + "<br>"
  document.getElementById("results").innerHTML += "Id: " + id  + "<br>"
  document.getElementById("results").innerHTML += "Program: " + program  + "<br>"
}
