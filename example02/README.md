# Example01 - Saving/Reading Simple Data

In this example, we save more than 1 item to the database
## Saving

1. Get data from user interface: `let x = document.getElementById(...).value`
2. Put data into localStorage:  `localStorage.setItem("____","_____")`
3. Repeat steps 1 & 2 until all your data is saved
3. Done!

## Reading

1. Get item from localStorage:  `let item = localStorage.getItem("____")`
2. Repeat until all items retrieved
3. Done!
4. Optional: If you get a number out of localStorage, remember to CAST the number to an int/float:  `parseInt()` or `parseFloat()`
