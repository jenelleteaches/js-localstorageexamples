
// global array to store names
let names = []

function save() {

  // 1. get name from user input box
  let name = document.getElementById("nameInput").value;

  if (name.length == 0) {
    // user didn't enter anything, so show error message and exit
    document.getElementById("results").innerHTML = "Error: You must enter all information"
    return
  }

  // add the name to the array
  names.push(name);

  // save the array to local storage.
  // To save an array, you must CONVERT the arrary -> string
  // Then save the string into localstorage
  let stringArray = JSON.stringify(names)
  localStorage.setItem("studentNames", stringArray);

  console.log(name + " saved to database");
  console.log(localStorage.getItem("studentNames"))

  document.getElementById("results").innerHTML = name + " saved to database."
}
