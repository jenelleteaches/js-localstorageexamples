// Try to get names from localStorage
let names = localStorage.getItem("studentNames");

// check if names is null
if (names == undefined) {
  // no items found in local storage
  document.getElementById("results").innerHTML = "No information in database."
}
else {

  // the array of names comes back as a STRING
  // therefore, you must convert STRING --> ARRAY before using it
  // --------

  // convert names to an array
  let students = JSON.parse(names);

  // loop through each item in the array and output it to the screen
  for (let i = 0; i < students.length; i++) {
    document.getElementById("results").innerHTML += "Name: " + students[i] + "<br>"
  }

}
