function save() {
  // get data from user interface
  let name = document.getElementById("nameInput").value;
  let id = document.getElementById("idInput").value;
  let prog = document.getElementById("programInput").value;

  // error handling
  if (name.length == 0 || id.length == 0 || prog.length == 0) {
    // user didn't enter anything, so show error message and exit
    document.getElementById("results").innerHTML = "Error: You must enter all information"
    return
  }

  // create a student object
  let student = {
    "studentName": name,
    "studentId": id,
    "program": prog
  }

  // add the student object to the array

  // save the dictionary to local storage.
  // To save an dictionary, you must CONVERT the arrary -> dictionary
  // Then save the string into localstorage
  let x = JSON.stringify(student)
  localStorage.setItem("student", x);

  // debugging
  console.log(name + " saved to database");
  console.log(localStorage.getItem("student"))

  // output confirmation message to user interface
  document.getElementById("results").innerHTML = name + " saved to database."
}
