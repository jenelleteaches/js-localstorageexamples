// get students from localStorage
let studentObject = localStorage.getItem("student");
if (studentObject == undefined) {
  // no items found in local storage
  document.getElementById("results").innerHTML = "No information in database."
}
else {

  // the dicitonary of student info comes back as a STRING
  // therefore, you must convert STRING --> dictionary before using it
  // --------

  // conver names to an dictionary
  let student = JSON.parse(studentObject);

  // get individual data points out of the dictionary
  document.getElementById("results").innerHTML = "Name: " + student["studentName"] + "<br>"
  document.getElementById("results").innerHTML += "ID: " + student["studentId"] + "<br>"
  document.getElementById("results").innerHTML += "Program: " + student["program"] + "<br><br>"

}
