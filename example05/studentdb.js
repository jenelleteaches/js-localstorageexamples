// Make a global array variable to store your students
let students = []

function save() {

  // Get the data from user interface
  let name = document.getElementById("nameInput").value;
  let id = document.getElementById("idInput").value;
  let prog = document.getElementById("programInput").value;


  if (name.length == 0 || id.length == 0 || prog.length == 0) {
    // user didn't enter anything, so show error message and exit
    document.getElementById("results").innerHTML = "Error: You must enter all information"
    return
  }

  // create a student object
  let student = {
    "studentName": name,
    "studentId": id,
    "program": prog
  }

  // add the name to the array
  students.push(student);

  // save the array to local storage.
  // To save an array, you must CONVERT the arrary -> string
  // Then save the string into localstorage
  let stringArray = JSON.stringify(students)
  localStorage.setItem("students", stringArray);

  console.log(name + " saved to database");
  console.log(localStorage.getItem("students"))

  document.getElementById("results").innerHTML = name + " saved to database."
}
