// get students from localStorage
let studentsArray = localStorage.getItem("students");

if (studentsArray == undefined) {
  // no items found in local storage
  document.getElementById("results").innerHTML = "No information in database."
}
else {

  // the array of names comes back as a STRING
  // therefore, you must convert STRING --> ARRAY before using it
  // --------

  // conver names to an array
  let students = JSON.parse(studentsArray);

  // loop through each item in the array and output it to the screen
  for (let i = 0; i < students.length; i++) {
    document.getElementById("results").innerHTML += "Name: " + students[i]["studentName"] + "<br>"
    document.getElementById("results").innerHTML += "ID: " + students[i]["studentId"] + "<br>"
    document.getElementById("results").innerHTML += "Program: " + students[i]["program"] + "<br><br>"
  }

}
