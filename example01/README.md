# Example01 - Saving/Reading Simple Data

## Saving

1. Get data from user interface: `let x = document.getElementById(...).value`
2. Put data into localStorage:  `localStorage.setItem("____","_____")`
3. Done!

## Reading

1. Get item from localStorage:  `localStorage.getItem("____")`
2. Done!
3. Optional: If you get a number out of localStorage, remember to CAST the number to an int/float:  `parseInt()` or `parseFloat()`


