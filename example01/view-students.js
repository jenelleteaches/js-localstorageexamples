// 1. Get the name out of localStorage
let name = localStorage.getItem("studentName");

// 2. REQUIRED ERROR HANDLING:  Check that the "studentName" key
// exists in the dictionary
if (name === undefined) {
  // 3a. no items found in local storage, so show error message in UI
  document.getElementById("results").innerHTML = "No students in database."
}
else {
  // 3b. 'studentName' key exists in localStorage, so output it to UI
  document.getElementById("results").innerHTML = name
}
