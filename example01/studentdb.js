// This function gets run when the person presses the SAVE button in the UI
function save() {

  // 1. Get what the person typed in the textbox
  let name = document.getElementById("userInput").value;

  // 2. OPTIONAL:  Do some error handling to check if the results are blank
  if (name.length == 0) {
    // user didn't enter anything, so show error message and exit
    document.getElementById("results").innerHTML = "Error: Please enter a name"
    return
  }

  // 3. Add to local storage
  localStorage.setItem("studentName", name);
  // 3a. OPTIONAL DEBUG CODE
  console.log(name + " saved to database");

  // 4. Notify the user that the person was saved to the database
  document.getElementById("results").innerHTML = name + " saved to database."
}
